import PieChart from '@/components/PieChart';
import AuthComponent from '@/pages/auth/AuthComponent';
import ForgotPassword from '@/pages/auth/ForgotPass';
import SignIn from '@/pages/auth/SignIn';
import SignUp from '@/pages/auth/SignUp';
import { Suspense } from 'react';

const dashboardRoutes = {
  children: [
    {
      index: true,
      path: 'pie-chart',
      element: (
        <Suspense fallback={<div>Loading...</div>}>
          <PieChart />
        </Suspense>
      ),
    },
  ],
};

const authRoutes = {
  children: [
    {
      index: true,
      path: 'auth',
      element: (
        <Suspense fallback={<div>Loading...</div>}>
          <AuthComponent />
        </Suspense>
      ),
    },
    {
      index: true,
      path: 'signin',
      element: (
        <Suspense fallback={<div>Loading...</div>}>
          <SignIn />
        </Suspense>
      ),
    },
    {
      index: true,
      path: 'signup',
      element: (
        <Suspense fallback={<div>Loading...</div>}>
          <SignUp />
        </Suspense>
      ),
    },
    {
      index: true,
      path: 'forgot-pass',
      element: (
        <Suspense fallback={<div>Loading...</div>}>
          <ForgotPassword />
        </Suspense>
      ),
    },
  ],
};

export const dashboard = [dashboardRoutes];
export const authPage = [authRoutes];
