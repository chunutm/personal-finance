import Dashboard from '@/layouts/Dashboard';
import Auth from '@/layouts/Auth';
import { createBrowserRouter } from 'react-router-dom';
import NotFound from '@/pages/NotFound';
import { dashboard, authPage } from './index';

const [dashboardRoutes] = dashboard;
const [authRoutes] = authPage;
const router = createBrowserRouter([
  {
    path: '/',
    element: <Dashboard />,
    errorElement: <NotFound />,
    children: dashboardRoutes.children,
  },
  {
    path: '/auth',
    element: <Auth />,
    errorElement: <NotFound />,
    children: authRoutes.children,
  },
]);

export default router;
