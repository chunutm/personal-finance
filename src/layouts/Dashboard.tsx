import Image from '@/assets/img';
import { BiSearch } from 'react-icons/bi';
import { useState } from 'react';
import { CiMail } from 'react-icons/ci';
import { BsBell } from 'react-icons/bs';
import { Outlet } from 'react-router-dom';
import Avatar from '@/components/common/Avatar';
import Navigation from './navbar/Navigation';

function Dashboard() {
  const [showSearch, setShowSearch] = useState(false);
  return (
    <div className="flex w-full">
      <div className="border-r-[1px] border-slate-600 border-solid">
        <Navigation />
      </div>
      <div className="flex flex-col w-full">
        <div className="relative w-full flex py-3 bg-[#2C2C2C] justify-between px-4">
          <div className="text-white">Dashboard</div>
          <div className="flex gap-3 items-center">
            {showSearch ? (
              <div
                onMouseMove={() => setShowSearch(true)}
                onMouseLeave={() => setShowSearch(false)}
                className="md-form relative md-bg-grey-100 md-outline md-outline-blue md-rounded"
              >
                <input
                  type="text"
                  id="search-input"
                  className="md-input relative w-80 rounded outline-none pl-4"
                />
                <BiSearch className="w-6 text-white cursor-pointer absolute right-2 transform -translate-y-1/2 top-1/2" />
              </div>
            ) : (
              <BiSearch
                onMouseMove={() => setShowSearch(true)}
                onMouseLeave={() => setShowSearch(false)}
                className="w-6 text-white cursor-pointer"
              />
            )}
            <BsBell className="w-6 text-white cursor-pointer" />
            <CiMail className="w-6 text-white cursor-pointer" />
            <Avatar src={Image.avatar_2} alt="avatar" size={6} />
          </div>
        </div>
        <div className="flex pl-4 py-4 h-full">
          <Outlet />
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
