import { Formik, Field, Form } from 'formik';
import Button from '@mui/material/Button';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import Image from '@/assets/img';

function ForgotPassword() {
  const validateEmail = () => {};
  const defaultValue = {
    email: '',
  };

  const [valueCustomInput] = useState(defaultValue);

  const backgroundImageStyle = {
    backgroundImage: `url(${Image.forgot_img})`,
  };
  return (
    <section
      className="bg-gray-50 dark:bg-gray-900 bg-gray-50 dark:bg-gray-900 bg-cover flex items-center justify-center"
      style={backgroundImageStyle}
    >
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <Link
          to="/"
          className="flex items-center mb-6 text-4xl font-semibold text-gray-900 dark:text-white"
        >
          <img
            className="w-8 h-8 mr-2"
            src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/logo.svg"
            alt="logo"
          />
          Finance
        </Link>
        <div className="w-full p-6 bg-white rounded-lg shadow dark:bg-opacity-70 dark:border md:mt-0 sm:max-w-md dark:bg-gray-800 dark:border-gray-700 sm:p-8">
          <h2 className="mb-1 text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
            Change Password
          </h2>

          <Formik
            initialValues={valueCustomInput}
            onSubmit={(values, { resetForm }) => {
              resetForm({ values: defaultValue });
            }}
          >
            <Form className="mt-4 space-y-4 lg:mt-5 md:space-y-5">
              <div className="mb-4">
                <Field
                  name="email"
                  type="text"
                  placeholder="Enter your email address..."
                  validate={validateEmail}
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                />
              </div>
              <div className="flex justify-center flex-col items-center">
                <p className="text-center">Or reset password by phone number</p>
                <button
                  type="button"
                  className="flex items-center py-2 px-4 w-48 mt-2 text-sm uppercase rounded bg-white hover:bg-gray-100 text-indigo-500 border border-transparent hover:border-transparent hover:text-gray-700 shadow-md hover:shadow-lg font-medium transition transform hover:-translate-y-0.5"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="#007bff"
                    stroke="#007bff"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-phone mr-4"
                    width="24"
                    height="24"
                  >
                    <path d="M23 3v18c0 .6-.4 1-1 1-1.2 0-2.5-.2-3.8-.8-2-1-4-1.5-6.2-1.5-2.2 0-4.2.5-6.2 1.5C3.5 20.8 2.2 21 1 21c-.6 0-1-.4-1-1V3c0-.6.4-1 1-1h22c.6 0 1 .4 1 1z" />
                  </svg>
                  Phone-number
                </button>
              </div>
              <Button className="w-full text-white bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                Send email to change password
              </Button>
            </Form>
          </Formik>
        </div>
      </div>
    </section>
  );
}

export default ForgotPassword;
