import Image from '@/assets/img';
import { Field, Form, Formik } from 'formik';
import { useState } from 'react';
import { Link } from 'react-router-dom';

function SignUp() {
  const defaultValue = {
    username: '',
    password: '',
  };
  const [valueCustomInput] = useState(defaultValue);

  const isValidEmail = (email: string) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  const validateUsername = (value: string) => {
    let errorMessage;
    if (!value) {
      errorMessage = 'Required';
    } else if (value.length < 8) {
      errorMessage = 'Username must be 8 characters or greater';
    }
    return errorMessage;
  };

  const validateEmail = (value: string) => {
    let errorMessage;
    if (!value) {
      errorMessage = 'Required';
    } else if (!isValidEmail(value)) {
      errorMessage = 'The Email is invalid format';
    }
    return errorMessage;
  };

  const validatePassword = (value: string) => {
    let errorMessage;
    if (!value) {
      errorMessage = 'Required';
    }
    return errorMessage;
  };
  const validatePhoneNumber = (value: string) => {
    const regex = /^(03|05|07|08|09)(\d{8})$/;
    return regex.test(value);
  };
  const validateConfirmPassword = () => {};
  return (
    <section className="h-screen">
      <div className="h-full px-6 text-neutral-800 dark:text-neutral-200">
        <div className="g-6 flex h-full flex-wrap items-center justify-center lg:justify-between">
          <div className="shrink-1 mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 md:shrink-0 lg:w-6/12 xl:w-6/12">
            <img
              src="https://tecdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
              className="w-full"
              alt="Sampleimage"
            />
          </div>
          <div className="mb-12 md:mb-0 md:w-8/12 lg:w-5/12 xl:w-5/12">
            <Formik
              initialValues={valueCustomInput}
              validateOnBlur
              validateOnChange
              onSubmit={(values, { resetForm }) => {
                resetForm({ values: defaultValue });
              }}
            >
              <Form>
                <div className="flex flex-row items-center justify-center lg:justify-center">
                  <p className="mb-0 mr-4 text-2xl font-extrabold">
                    We are the Finance System
                  </p>
                </div>

                <div className="my-4 flex items-center before:mt-0.5 before:flex-1 before:border-t before:border-neutral-300 after:mt-0.5 after:flex-1 after:border-t after:border-neutral-300">
                  <img
                    src={Image.logo_finance}
                    width="100px"
                    alt="LOGO Finance"
                    className="bor"
                  />
                </div>
                <div className="relative mb-6" data-te-input-wrapper-init>
                  <Field
                    name="email"
                    type="text"
                    placeholder="Enter your email"
                    validate={validateEmail}
                    className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                  />
                </div>

                <div className="relative mb-6" data-te-input-wrapper-init>
                  <Field
                    name="username"
                    type="text"
                    placeholder="Enter your username"
                    validate={validateUsername}
                    className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                  />
                </div>

                <div className="relative mb-6" data-te-input-wrapper-init>
                  <Field
                    name="phoneNumber"
                    type="text"
                    placeholder="Enter your phone number"
                    validate={validatePhoneNumber}
                    className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                  />
                </div>

                <div className="relative mb-6" data-te-input-wrapper-init>
                  <Field
                    name="password"
                    type="text"
                    placeholder="Enter your password"
                    validate={validatePassword}
                    className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                  />
                </div>

                <div className="relative mb-6" data-te-input-wrapper-init>
                  <Field
                    name="confirmPassword"
                    type="text"
                    placeholder="Enter confirm password"
                    validate={validateConfirmPassword}
                    className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                  />
                </div>

                <div className="text-center lg:text-left">
                  <div className="w-full flex justify-center">
                    <button
                      type="button"
                      className="inline-block  rounded bg-primary px-7 pt-3 pb-2.5 text-sm font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                      data-te-ripple-init
                      data-te-ripple-color="light"
                    >
                      Register
                    </button>
                  </div>
                  <p className="mt-2 mb-0 pt-1 text-sm font-semibold">
                    Have an account?
                    <Link
                      to="/auth/signin"
                      className="text-danger ml-2 transition duration-150 ease-in-out hover:text-danger-600 focus:text-danger-600 active:text-danger-700"
                    >
                      Login
                    </Link>
                  </p>
                </div>
              </Form>
            </Formik>
          </div>
        </div>
      </div>
    </section>
  );
}

export default SignUp;
