import Image from '@/assets/img';
import { Formik, Field, ErrorMessage, Form } from 'formik';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import CustomError from './CustomError';

function SignIn() {
  const defaultValue = {
    username: '',
    password: '',
  };
  const [valueCustomInput, setValueInput] = useState(defaultValue);
  const [isShowPass, setIsShowPass] = useState(false);

  useEffect(() => {
    const obj = {
      username: '',
      email: '',
      phoneNumber: '',
      password: '',
      confirmPassword: '',
    };
    setValueInput(obj);
  }, []);

  const validateUsername = (value: string) => {
    let errorMessage;
    if (!value) {
      errorMessage = 'Required';
    } else if (value.length < 8) {
      errorMessage = 'Username must be 8 characters or greater';
    }
    return errorMessage;
  };

  const validatePassword = (value: string) => {
    let errorMessage;
    if (!value) {
      errorMessage = 'Required';
    }
    return errorMessage;
  };

  const handleShowPassword = () => {
    setIsShowPass(!isShowPass);
  };

  return (
    <section className="relative flex flex-wrap lg:h-screen lg:items-center">
      <div className="w-full px-4 py-12 sm:px-6 sm:py-16 lg:w-1/2 lg:px-8 lg:py-24">
        <div className="mx-auto max-w-lg text-center">
          <h1 className="text-2xl font-bold sm:text-3xl">Welcome back!</h1>

          <p className="mt-4 text-gray-500">
            Please log in to access the personal finance management product.
          </p>
        </div>
        <Formik
          initialValues={valueCustomInput}
          onSubmit={(values, { resetForm }) => {
            resetForm({ values: defaultValue });
          }}
        >
          <Form className="mx-auto mt-8 mb-0 max-w-md space-y-4">
            <div>
              <div className="relative">
                <Field
                  name="username"
                  type="text"
                  placeholder="Enter your username or email"
                  validate={validateUsername}
                  className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                />
                <span className="absolute inset-y-0 right-0 grid place-content-center px-4">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 text-gray-400"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                    />
                  </svg>
                </span>
              </div>
              <ErrorMessage name="username" component={CustomError} />
            </div>

            <div>
              <div className="relative">
                <Field
                  name="password"
                  type={isShowPass ? 'text' : 'password'}
                  placeholder="Enter your password"
                  validate={validatePassword}
                  className="flex flex-col w-full border w-full rounded-lg border-gray-200 p-4 pr-12 text-sm shadow-sm"
                />
                <span
                  className="absolute inset-y-0 right-0 grid place-content-center px-4"
                  onClick={handleShowPassword}
                  role="button"
                  tabIndex={0}
                  onKeyPress={handleShowPassword}
                >
                  {isShowPass ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      className="h-4 w-4 text-gray-400"
                      fill="currentColor"
                    >
                      <path fill="none" d="M0 0h24v24H0V0z" />
                      <path d="M22.71 21.29l-19-19A1 1 0 0 0 2.29 3.71l19 19a1 1 0 0 0 1.42-1.42zM12 8c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 6c-1.11 0-2-.89-2-2s.89-2 2-2 2 .89 2 2-.89 2-2 2z" />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-4 w-4 text-gray-400"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                      />
                    </svg>
                  )}
                </span>
              </div>
              <ErrorMessage name="password" component={CustomError} />
            </div>

            <div className="flex items-center justify-between">
              <p className="text-sm text-gray-500 w-full">
                <div className="flex justify-between items-center">
                  <button
                    type="submit"
                    className="inline-block rounded-lg bg-blue-500 w-40 px-5 py-3 text-sm font-medium text-white"
                  >
                    Sign in
                  </button>
                  <Link
                    className="underline ml-1 w-full flex justify-end block"
                    to="/auth/forgot-pass"
                  >
                    Forgot your password?
                  </Link>
                </div>
                <p className="flex justify-between flex-col block items-center">
                  <p className="flex justify-center block">Or login with</p>
                  <p className="flex justify-evenly items-center w-full flex-col block mt-4">
                    <button
                      type="button"
                      className="flex w-6/12 items-center py-2 px-4 text-sm uppercase rounded bg-white hover:bg-gray-100 text-indigo-500 border border-transparent hover:border-transparent hover:text-gray-700 shadow-md hover:shadow-lg font-medium transition transform hover:-translate-y-0.5"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 16 16"
                        className="w-6 h-6 mr-3"
                      >
                        <path
                          fillRule="evenodd"
                          d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"
                        />
                      </svg>
                      Github
                    </button>
                    <button
                      type="button"
                      className="flex w-6/12 mt-4 items-center py-2 px-4 text-sm uppercase rounded bg-white hover:bg-gray-100 text-indigo-500 border border-transparent hover:border-transparent hover:text-gray-700 shadow-md hover:shadow-lg font-medium transition transform hover:-translate-y-0.5"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="w-6 h-6 mr-3"
                        viewBox="0 0 48 48"
                      >
                        <path
                          fill="#fbc02d"
                          d="M43.611 20.083H42V20H24v8h11.303c-1.649 4.657-6.08 8-11.303 8-6.627 0-12-5.373-12-12s5.373-12 12-12c3.059 0 5.842 1.154 7.961 3.039l5.657-5.657C34.046 6.053 29.268 4 24 4 12.955 4 4 12.955 4 24s8.955 20 20 20 20-8.955 20-20c0-1.341-.138-2.65-.389-3.917z"
                        />
                        <path
                          fill="#e53935"
                          d="m6.306 14.691 6.571 4.819C14.655 15.108 18.961 12 24 12c3.059 0 5.842 1.154 7.961 3.039l5.657-5.657C34.046 6.053 29.268 4 24 4 16.318 4 9.656 8.337 6.306 14.691z"
                        />
                        <path
                          fill="#4caf50"
                          d="M24 44c5.166 0 9.86-1.977 13.409-5.192l-6.19-5.238A11.91 11.91 0 0 1 24 36c-5.202 0-9.619-3.317-11.283-7.946l-6.522 5.025C9.505 39.556 16.227 44 24 44z"
                        />
                        <path
                          fill="#1565c0"
                          d="M43.611 20.083 43.595 20H24v8h11.303a12.04 12.04 0 0 1-4.087 5.571l.003-.002 6.19 5.238C36.971 39.205 44 34 44 24c0-1.341-.138-2.65-.389-3.917z"
                        />
                      </svg>
                      Google
                    </button>
                  </p>
                </p>
              </p>
            </div>
            <div className="flex items-center justify-between">
              <p className="text-sm text-gray-500">
                No account?
                <Link className="underline ml-1" to="/auth/signup">
                  Sign up
                </Link>
              </p>
            </div>
          </Form>
        </Formik>
      </div>

      <div className="relative h-64 w-full sm:h-96 lg:h-full lg:w-1/2">
        <img
          alt="Welcome"
          src={Image.auth_login}
          className="absolute inset-0 h-full w-full object-cover"
        />
      </div>
    </section>
  );
}
export default SignIn;
