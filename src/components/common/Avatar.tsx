import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

interface IAvatarProps {
  src: string;
  alt: string;
  size: number;
}

function Avatar({ src, alt, size }: IAvatarProps) {
  const [avatarSrc, setAvatarSrc] = useState(src);
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const openProfile = Boolean(anchorEl);
  const handleClick: React.MouseEventHandler<HTMLImageElement> = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSettingAccount = () => {
    handleClose();
  };

  const handleLogout = () => {
    navigate('/auth/signin');
  };

  const handleSetting = () => {
    handleClose();
  };

  const handleError = () => {
    setAvatarSrc('https://via.placeholder.com/150');
  };
  return (
    <div className="cursor-pointer mr-4">
      <img
        src={avatarSrc}
        alt={alt}
        onError={handleError}
        className={`w-${size} h-${size} rounded-full`}
        key={size}
        onClick={handleClick}
        aria-hidden="true"
      />
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={openProfile}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={handleSettingAccount}>My account</MenuItem>
        <MenuItem onClick={handleSetting}>Setting</MenuItem>
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
      </Menu>
    </div>
  );
}

export default Avatar;
