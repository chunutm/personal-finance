import avatar_2 from './avatar/avatar-2.jpg';
import avatar_3 from './avatar/avatar-3.jpg';
import avatar_4 from './avatar/avatar-4.jpg';
import avatar_5 from './avatar/avatar-5.jpg';
import avatar from './avatar/avatar.jpg';
import login_img from './avatar/login_img.jpg';
import forgot_img from './forgot_img.jpg';
import auth_login from './auth_login_img.jpg';
import finance_logo from './finance_logo.jpg';
import logo_finance from './logo_finance.png';

const Image = {
  avatar_2,
  avatar_3,
  avatar_4,
  avatar_5,
  avatar,
  login_img,
  forgot_img,
  auth_login,
  finance_logo,
  logo_finance,
};
export default Image;
