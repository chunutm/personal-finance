/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

interface ILoadingState {
  isLoad: boolean;
  isLoadingFullScreen: boolean;
}

const initialState: ILoadingState = {
  isLoad: false,
  isLoadingFullScreen: false,
};

interface ISetLoadingProps {
  isLoadingFullScreen: boolean;
}

const loadingSlice = createSlice({
  name: 'loading',
  initialState,
  reducers: {
    changeLoading: (state, action) => {
      state.isLoad = action.payload;
    },
    setLoading: (state, action: { payload: ISetLoadingProps }) => {
      const { isLoadingFullScreen } = action.payload;
      state.isLoad = false;
      state.isLoadingFullScreen = isLoadingFullScreen;
    },
  },
});

export const { changeLoading } = loadingSlice.actions;
export const loadingSelector = (state: ILoadingState) => {
  return state.isLoad;
};
export default loadingSlice.reducer;
