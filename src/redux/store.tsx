import { configureStore } from '@reduxjs/toolkit';
import LoadingReducer from './reducers/LoadingReducer';

const store = configureStore({
  reducer: {
    loading: LoadingReducer,
  },
});

export default store;
